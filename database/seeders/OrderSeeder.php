<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'id' => Str::uuid(),
            'full_name' => Str::random(rand(32, 64)),
            'phone' => rand(8900000000, 8999999999),
            'price' => rand(1, 9223372036854775807),
            'delivery_address' => Str::random(255),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
