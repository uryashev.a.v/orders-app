<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property string $id
 * @property string $full_name
 * @property integer $price
 * @property integer $phone
 * @property string $delivery_address
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin Builder
 */
class Order extends Model
{
    use HasFactory;

    protected $keyType = 'string';

    protected $perPage = 30;

    protected $fillable = [
        'id',
        'full_name',
        'price',
        'phone',
        'delivery_address',

    ];

    protected $casts = [
        'id' => 'string',
        'price' => 'integer',
        'phone' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
