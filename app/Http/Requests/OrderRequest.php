<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string|min:3|max:255',
            'phone' => 'required|numeric|digits:10|starts_with:7,8',
            'delivery_address' => 'required|string|min:3|max:255',
            'price' => 'required|integer|min:1',
        ];
    }
}
