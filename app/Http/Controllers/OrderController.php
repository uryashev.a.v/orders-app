<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderListRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderCollectionResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Symfony\Component\HttpFoundation\Response as HTTP_RESPONSE;

class OrderController extends Controller
{
    public function create(OrderRequest $request, OrderService $orderService): OrderResource
    {
        return OrderResource::make(
            $orderService->create(app(Order::class)->fill([
                'full_name' => $request->input('full_name'),
                'price' => $request->input('price'),
                'phone' => $request->input('phone'),
                'delivery_address' => $request->input('delivery_address'),
            ]))
        );
    }

    public function getById($id, OrderService $orderService): OrderResource
    {
        $order = $orderService->getById($id);
        if (!$order) {
            abort(response([
                "type" => "ORDER_NOT_FOUND",
                "code" => HTTP_RESPONSE::HTTP_NOT_FOUND,
                "message" => "Товар не найден."
            ], HTTP_RESPONSE::HTTP_NOT_FOUND));
        }
        return OrderResource::make($order);
    }

    public function getAll(OrderListRequest $request, OrderService $orderService): OrderCollectionResource
    {
        return OrderCollectionResource::make($orderService->getAll(
            (int)$request->input('page', 1),
            $request->input('perPage'),
        ));
    }

    public function update(OrderRequest $request, $id, OrderService $orderService): OrderResource
    {
        $order = $orderService->getById($id);
        if ($order === null) {
            abort(response(["message" => 'Order mot found.'], HTTP_RESPONSE::HTTP_NOT_FOUND));
        }

        return OrderResource::make(
            $orderService->update($order->id, app(Order::class)->fill([
                'full_name' => $request->input('full_name'),
                'price' => $request->input('price'),
                'phone' => $request->input('phone'),
                'delivery_address' => $request->input('delivery_address'),
            ]))
        );
    }
}
