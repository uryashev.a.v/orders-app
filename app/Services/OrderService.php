<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class OrderService
{
    public function create(Order $order): Order
    {
        return Order::create([
            'id' => Str::uuid(),
            'full_name' => $order->full_name,
            'price' => $order->price,
            'phone' => $order->phone,
            'delivery_address' => $order->delivery_address,
        ]);
    }

    public function getById(string $id): ?Order
    {
        return Order::where(['id' => $id])->first();
    }

    public function getAll(int $page = 1, int $perPage = null): LengthAwarePaginator
    {
        return Order::paginate($perPage, ['*'], 'ordersPage', $page);
    }

    public function update(string $id, Order $order): ?Order
    {
        Order::where(['id' => $id])->update([
            'full_name' => $order->full_name,
            'price' => $order->price,
            'phone' => $order->phone,
            'delivery_address' => $order->delivery_address,
        ]);

        return $this->getById($id);
    }

}
