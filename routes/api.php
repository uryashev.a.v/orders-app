<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/orders')->as('orders.')->group(function () {
    Route::post('/', [OrderController::class, 'create'])->name('create');
    Route::get('/', [OrderController::class, 'getAll'])->name('getAll');
    Route::get('/{id}', [OrderController::class, 'getById'])->name('getById');
    Route::patch('/{id}', [OrderController::class, 'update'])->name('update');
});
