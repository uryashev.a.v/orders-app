<?php

namespace Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateOrderTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_order()
    {
        $data = [
            'phone' => rand(7000000000, 8999999999),
            'price' => rand(1, 9223372036854775807),
            'delivery_address' => Str::random(255),
            'full_name' => Str::random(255),
        ];
        $response = $this->postJson('/api/orders', $data);
        $response
            ->assertStatus(201)
            ->assertJson(fn(AssertableJson $json) => $json
                ->where('data.phone', $data['phone'])
                ->where('data.price', $data['price'])
                ->where('data.delivery_address', $data['delivery_address'])
                ->where('data.full_name', $data['full_name'])
            );
    }

}
