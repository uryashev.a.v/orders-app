<?php

namespace Feature;

use App\Models\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateOrderTest extends TestCase
{
    use DatabaseTransactions;

    public function test_update_order_200()
    {
        $order = Order::first();
        $this->assertNotNull($order);
        $data = [
            'phone' => rand(7000000000, 8999999999),
            'price' => rand(1, 9223372036854775807),
            'delivery_address' => Str::random(255),
            'full_name' => Str::random(255),
        ];

        $response = $this->patch("api/orders/$order->id", $data);
        $response
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json
                ->where('data.phone', $data['phone'])
                ->where('data.price', $data['price'])
                ->where('data.delivery_address', $data['delivery_address'])
                ->where('data.full_name', $data['full_name'])
            );
    }

    public function test_order_not_exist()
    {
        $id = Str::uuid();
        $response = $this->patch("api/orders/$id", [
            'phone' => rand(7000000000, 8999999999),
            'price' => rand(1, 9223372036854775807),
            'delivery_address' => Str::random(255),
            'full_name' => Str::random(255),
        ]);
        $response
            ->assertStatus(404)
            ->assertJsonFragment(['message' => 'Order mot found.']);
    }
}
