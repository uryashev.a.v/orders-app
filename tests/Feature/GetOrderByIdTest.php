<?php

namespace Feature;

use App\Models\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class GetOrderByIdTest extends TestCase
{
    use DatabaseTransactions;

    public function test_get_order_by_id_success()
    {
        $order = Order::first();
        $this->assertNotNull($order);

        $response = $this->get("/api/orders/$order->id");

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => $order->id,
                'full_name' => $order->full_name,
                'price' => $order->price,
                'phone' => $order->phone,
                'created_at' => $order->created_at,
                'updated_at' => $order->updated_at
            ]);
    }

    public function test_order_not_exist()
    {
        $id = Str::uuid();
        $response = $this->patch("api/orders/$id", [
            'phone' => rand(7000000000, 8999999999),
            'price' => rand(1, 9223372036854775807),
            'delivery_address' => Str::random(255),
            'full_name' => Str::random(255),
        ]);
        $response
            ->assertStatus(404)
            ->assertJsonFragment([
                'message' => 'Order mot found.'
            ]);
    }
}
